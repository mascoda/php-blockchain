<?php

declare(strict_types=1);

namespace mascoda\phpBlockchain;

use Mascoda\PhpBlockchain\Block;
use Mascoda\PhpBlockchain\Storage;
use Mascoda\PhpBlockchain\Wallet;
use Mascoda\PhpBlockchain\Mempool;

class Chain
{

    public int $difficulty = 2;

    /**
     * __construct
     * creates a genesis block if not exists
     *
     * @return void
     */
    function __construct()
    {
        if (count($this->getBlocks()) < 1) :
            self::createGenesisBlock();
        endif;
    }

    /**
     * setDifficulty 
     * the difficulty is a measure of how difficult it is to mine a Bitcoin block, or in more technical terms,
     * to find a hash below a given target.
     *
     * @param  int $difficulty
     * @return void
     */
    public function setDifficulty(int $difficulty): void
    {
        $this->setConfig("difficulty", $difficulty);
    }

    /**
     * returned the current difficulty
     *
     * @return int difficulty
     */
    public function getDifficulty(): int
    {
        return intval($this->get("difficulty"));
    }

    /**
     * create a genesis block
     *
     * @return void
     */
    private function createGenesisBlock(): void
    {
        $block = new Block();
        $new_block = $block->create();
        $new_block->addTransaction(array());
        $new_block->verify("000000000000");
        self::addBlock($new_block);
    }

    /**
     * addBlock
     *
     * @param  object $block
     * @return void
     */
    function addBlock($block): void
    {
        self::transfer($block->transactions);

        $storage = new Storage("blockchain");
        $storage->append("blocks", $block);

        $mempool = new Mempool();
        $mempool->clear();
    }

    /**
     * returned an array with all blocks
     *
     * @return array
     */
    function getBlocks(): array
    {
        $storage = new Storage("blockchain");
        $blockchain = $storage->get();

        return $blockchain->blocks;
    }

    /**
     * getIndex
     *
     * @return int last block index
     */
    public static function getIndex(): int
    {
        $storage = new Storage("blockchain");
        $blockchain = $storage->get();

        return count($blockchain->blocks);
    }

    /**
     * getLastBlockHash
     *
     * @return string hash from the last block in the blockchain
     */
    public static function getLastBlockHash(): string
    {
        $storage = new Storage("blockchain");
        $blockchain = $storage->get();
        if (count($blockchain->blocks) > 0) :
            return $blockchain->blocks[count($blockchain->blocks) - 1]->hash;
        endif;
        return str_repeat("0", 32);
    }


    /**
     * calculate all transactions from the block and update all wallets
     *
     * @param  array $transactions
     * @return void
     */
    function transfer($transactions): void
    {
        if (count($transactions) > 1) :
            $wallet = new Wallet();
            foreach ($transactions as $transaction) :
                $wallet->setValue($transaction->creditor, floatVal(0 - $transaction->amount));
                $wallet->setValue($transaction->debitor, floatVal(0 + $transaction->amount));
            endforeach;
        endif;
    }

    /**
     * set configuration parameter by key value
     * configuration is stored in blockchain.json in the <meta> object
     *
     * @param  string $key
     * @param  string $value
     * @return void
     */
    function setConfig($key, $value): void
    {
        $storage = new Storage("blockchain");
        $blockchain = $storage->get();
        $meta = (array) $blockchain->meta;
        $meta[$key] = $value;
        $storage->write("meta",  (object) $meta);
    }

    /**
     * get the configuration value by key
     *
     * @param  string $key
     * @return string
     */
    function get($key): string|int
    {
        $storage = new Storage("blockchain");
        $blockchain = $storage->get();
        $meta = (array) $blockchain->meta;
        return $meta[$key];
    }
}
