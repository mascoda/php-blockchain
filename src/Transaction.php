<?php

namespace mascoda\phpBlockchain;

use Mascoda\PhpBlockchain\Wallet;
use Mascoda\PhpBlockchain\Storage;
use Mascoda\PhpBlockchain\Hash;

class Transaction
{

    public string $tx;
    public int $timestamp;
    public float $amount;
    public string $creditor;
    public string $debitor;
    private $private_key;

    /**
     * __construct
     *
     * @param  mixed $creditor
     * @param  mixed $private_key
     * @param  mixed $debitor
     * @param  mixed $amount
     * @return void
     */
    function __construct($creditor, $private_key, $debitor, $amount)
    {
        $this->creditor = $creditor;
        $this->private_key = $private_key;
        $this->debitor = $debitor;
        $this->amount = $amount;
        $this->timestamp = time();
        $this->tx = $this->createTx();
    }

    /**
     * create a unique tx 
     *
     * @return string tx
     */
    private function createTx(): string
    {
        $hash = new Hash();
        return $hash->generate(serialize($this));
    }

    /**
     * add transaction to the mempool
     *
     * @return bool
     */
    function add(): bool
    {
        if ($this->isValid()) :
            $storage = new Storage("mempool");
            $storage->append("transactions", $this);
            return true;
        endif;
        return false;
    }

    /**
     * check is transaction is valid
     * returned true is valid
     * returned errorMessage (string) if not valid
     *
     * @return bool|string
     */
    function isValid(): string
    {
        if (!$this->validPrivateKey()) :
            return "invalid key";
        endif;
        if (!$this->validAmount()) :
            return "invalid amount";
        endif;
        if (!$this->validDebitor()) :
            return "invalid debitor walllet";
        endif;
        return true;
    }

    /**
     * check if the private key is valid
     *
     * @return bool true|valid false|invalid
     */
    private function validPrivateKey(): bool
    {
        $wallet = new Wallet();
        $creditor_wallet = $wallet->get($this->creditor);
        if ($creditor_wallet && $creditor_wallet->private_key == $this->private_key) :
            return true;
        endif;
        return false;
    }

    /**
     * check if the amount is valid
     *
     * @return bool true|valid false|invalid
     */
    private function validAmount(): bool
    {
        $wallet = new Wallet();
        $creditor_wallet = $wallet->get($this->creditor);
        if ($creditor_wallet->value >= $this->amount) :
            return true;
        endif;
        return false;
    }

    /**
     * check is the debitor is valid (valid public_key|wallet_id)
     *
     * @return bool true|valid false|invalid
     */
    private function validDebitor(): bool
    {
        $wallet = new Wallet();
        if ($wallet->get($this->debitor)) :
            return true;
        endif;
        return false;
    }

    /**
     * get all transaction from the mempool
     *
     * @return array
     */
    function get(): array
    {
        $storage = new Storage("mempool");
        $mempool = $storage->get();
        return $mempool->transactions;
    }
}
