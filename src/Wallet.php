<?php

namespace mascoda\phpBlockchain;

use Mascoda\PhpBlockchain\Hash;
use Mascoda\PhpBlockchain\Storage;

class Wallet
{
    public string $public_key;
    public string $private_key;
    public float  $value;
    public int  $timestamp;

    private float $defaultValue = 0.00;

    /**
     * create
     *
     * @return self
     */
    function create(): self
    {
        $this->public_key = $this->createPublicKey();
        $this->private_key = $this->createPrivateKey();
        $this->value = $this->defaultValue;
        $this->timestamp = time();

        $this->add();

        return $this;
    }

    /**
     * set the default value when a new wallet created
     *
     * @param  float $defaultValue
     * @return void
     */
    function setDefaultValue($defaultValue): void
    {
        $this->defaultValue = floatval($defaultValue);
    }

    /**
     * createPublicKey
     *
     * @return string
     */
    private function createPublicKey(): string
    {
        $hash = new Hash();
        return $hash->generate();
    }

    /**
     * createPrivateKey
     *
     * @return string
     */
    private function createPrivateKey(): string
    {
        $hash = new Hash();
        return $hash->generate();
    }

    /**
     * add the wallet to the storage
     *
     * @return void
     */
    private function add(): void
    {
        $storage = new Storage("wallet");
        $storage->append("wallets", $this);
    }

    /**
     * get the wallet by public_key
     * if not public_key exists, return all wallets
     *
     * @param  string $public_key
     * @return object
     */
    function get($public_key = false): object|bool
    {
        $storage = new Storage("wallet");
        $wallet = $storage->get();
        if ($public_key) :
            foreach ($wallet->wallets as $w) :
                if ($w->public_key == $public_key) :
                    return $w;
                endif;
            endforeach;
            return false;
        endif;
        return $wallet->wallets;
    }

    /**
     * set a value (amount of coins) for the wallet by public_key
     *
     * @param  string $public_key
     * @param  float $amount
     * @return void
     */
    function setValue($public_key, $amount): void
    {
        $storage = new Storage("wallet");
        $wallet = $storage->get();

        foreach ($wallet->wallets as $w) :
            if ($w->public_key == $public_key) :
                $w->value += $amount;
            endif;
            $calculated_wallets[] = $w;
        endforeach;
        $storage->write('wallets', $calculated_wallets);
    }
}
