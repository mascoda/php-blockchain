<?php

namespace mascoda\phpBlockchain;

use Mascoda\PhpBlockchain\Mempool;
use Mascoda\PhpBlockchain\Block;
use Mascoda\PhpBlockchain\Hash;


class Node
{
    
    /**
     * mining - simulate the mining process
     *
     * @return Block
     */
    function mining(): Block
    {
        $mempool = new Mempool();
        $unconfirmed_transactions = $mempool->get();

        $block = new Block();
        $block->create();

        foreach ($unconfirmed_transactions as $transaction) :
            $block->addTransaction($transaction);
        endforeach;


        $hash = new Hash();
        $blockString = serialize($unconfirmed_transactions);
        $computed_hash = $hash->generate($blockString);

        $blockchain = new Chain();
        // var_dump($blockchain->getDifficulty());

        do {
            $block->nonce += 1;
            $string_to_hash = $blockString . strval($block->nonce);
            $computed_hash = $hash->generate($string_to_hash);
        } while (!str_starts_with($computed_hash, str_repeat("0", $blockchain->getDifficulty())));


        $block->verify($computed_hash);

        return $block;
    }
}
