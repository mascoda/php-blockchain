<?php

namespace mascoda\phpBlockchain;

class Hash
{
    /**
     * generate a sha256 encrypted hash by the given string
     *
     * @param  string $stringToHash
     * @return string
     */
    function generate($stringToHash = false): string
    {
        if ($stringToHash === false) :
            $stringToHash = self::getRandomString();
        endif;

        return hash('sha256', $stringToHash);
    }

    /**
     * returned a random string
     *
     * @return string
     */
    function getRandomString(): string
    {
        return rand(11111, 99999) + intval(microtime()) + rand(11111, 99999);
    }
}
