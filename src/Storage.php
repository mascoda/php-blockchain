<?php

namespace mascoda\phpBlockchain;

class Storage
{

    public string $name;
    
    /**
     * __construct
     *
     * @param  string $name
     * @return void
     */
    function __construct($name)
    {
        $this->name = $name;
    }
    
    /**
     * decode and returned stored data
     *
     * @return object
     */
    function get(): object
    {
        $content = file_get_contents('data/' . $this->name . '.json', true);
        return json_decode($content);
    }
    
    /**
     * write and decode data by section
     *
     * @param  mixed $section
     * @param  mixed $data
     * @return void
     */
    function write($section, $data): void
    {
        $content = self::get();
        $content->$section = $data;

        file_put_contents('src/data/' . $this->name . '.json', json_encode($content));
    }
    
    /**
     * append and decode data by section
     *
     * @param  mixed $section
     * @param  mixed $append_data
     * @return void
     */
    function append($section, $append_data): void
    {
        $content = self::get();
        $content->$section[] = $append_data;
        $content = json_encode($content);
        file_put_contents('src/data/' . $this->name . '.json', $content);
    }
}
