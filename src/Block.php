<?php

namespace mascoda\phpBlockchain;

use Mascoda\PhpBlockchain\Chain;

class Block
{
    public int $index;
    public string $hash;
    public string $previous_hash;
    public int $timestamp;
    public array $transactions;
    public int $nonce = 0;

    /**
     * create a block
     *
     * @return self
     */
    function create(): self
    {
        $this->index = $this->getIndex();
        $this->previous_hash = $this->getPreviousHash();
        $this->timestamp = time();

        return $this;
    }

    /**
     * verify the block (mining) with a valid calculated hash
     *
     * @param  mixed $calculated_hash
     * @return void
     */
    function verify(?string $calculated_hash = null): void
    {
        $this->hash = $calculated_hash;
    }

    /**
     * add transaction to the block
     *
     * @param  array $transaction
     * @return void
     */
    function addTransaction(array $transaction): void
    {
        $this->transactions[] = $transaction;
    }

    /**
     * getTransactions
     *
     * @return array cointained all transactions in the block
     */
    function getTransactions(): array
    {
        return $this->transactions;
    }

    /**
     * get the last index of the blockchain block and increment with 1
     *
     * @return int
     */
    function getIndex(): int
    {
        return Chain::getIndex() + 1;
    }

    /**
     * get the hash from the last block in the blockchain
     *
     * @return string
     */
    function getPreviousHash(): string
    {
        return Chain::getLastBlockHash();
    }
}
