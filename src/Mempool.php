<?php

namespace mascoda\phpBlockchain;

use Mascoda\PhpBlockchain\Storage;

class Mempool
{    
    /**
     * add a transaction to the mempool
     *
     * @param  array|object $transaction
     * @return void
     */
    function add($transaction): void
    {
        $storage = new Storage("mempool");
        $storage->append("transactions", $transaction);
    }
    
    /**
     * get all transactions from the mempool
     *
     * @return array
     */
    function get(): array
    {
        $storage = new Storage("mempool");
        $mempool = $storage->get();
        return $mempool->transactions;
    }
    
    /**
     * clear the mempool (delete all the transactions)
     *
     * @return void
     */
    function clear(): void
    {
        $storage = new Storage("mempool");
        $storage->write("transactions", []);
    }
}
