<?php return array(
    'root' => array(
        'name' => 'mascoda/php-blockchain',
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'reference' => '5924d1a04c1143c975d47f084e9242fbdd3af820',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'mascoda/php-blockchain' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '5924d1a04c1143c975d47f084e9242fbdd3af820',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
