<?php
set_time_limit(600);

require_once __DIR__ . '/vendor/autoload.php';

use Mascoda\PhpBlockchain\Chain;
use Mascoda\PhpBlockchain\Block;
use Mascoda\PhpBlockchain\Mempool;
use Mascoda\PhpBlockchain\Wallet;
use Mascoda\PhpBlockchain\Hash;
use Mascoda\PhpBlockchain\Transaction;
use Mascoda\PhpBlockchain\Node;


// create an instance of blockchain
// created a genesis block if not exists
$blockchain = new Chain();

/*
* Difficulty Explanation. The difficulty is a measure of how difficult it is to mine a Bitcoin block, or in more technical terms, 
* to find a hash below a given target. A high difficulty means that it will take more computing power to mine the same 
* number of blocks, making the network more secure against attacks.
*
* Caution: A high value can have a huge impact on performance. It is recommended to test with the default value of 2.
* If the difficulty has a high value, the PHP script runtime may have to be increased. 
* To do this, you can use **set_time_limit(600);** at the beginning of the file.
*/
$blockchain->setDifficulty(2);
/*
* Use the setConfig method to add various attributes to the blockchain. You can add as many attributes as you want via key->value.
* Existing attributes are overwritten using the setConfig method.
* The attributes are permanently stored in the associated json file (blockchain.json).
*/
$blockchain->setConfig("coinname", "chaincoin");
$blockchain->setConfig("symbol", "cc");
$blockchain->setConfig("description", "Description of my blockchain");
/*
* The configured attributes can be loaded via the get method via the key.
*/
$blockchain->get("coinname");


/*
* A blockchain wallet is a digital wallet that allows users to store and manage their coins.
* For testing, it is therefore recommended to use the setDefaultValue function so that transactions between the wallets are possible.
*/
$wallet = new Wallet();

/*
* Set a default balance for the wallet (optional)
* (default amount of coins in the new wallet)
*/
$wallet->setDefaultValue(1.0);

/*
* Creates a new wallet with an associated public/private key pair. If no default value has been set, the wallet contains a sum of money of 0.
* For testing, it is therefore recommended to use the setDefaultValue function so that transactions between the wallets are possible.
*/
$new_wallet = $wallet->create();

echo "New wallet created:", "<br>";
echo "public_key : ", $new_wallet->public_key, "<br>";
echo "private_key: ", $new_wallet->private_key, "<br>";
echo "balance    : ", $new_wallet->value, "<br>";
echo "<br>";


/*
* A transaction is similar to a bank transfer. A defined sum of coins is exchanged between debtor and recipient. 
* The transfer is verified using the debtor's private key.
*/

// The public key from the creditor
$creditor = "2153949f752d8b4129118a867693ca740113e64a903c64a0feb42f9fffc48384";

// The private key from the creditor
$private_key = "ac7ccacd434a04490fd82af6a3a9a243999a122ba0d4046d078d8bfc10d5d019";

// The public key (wallet id) from the creditor
$debitor = "9855baa0e67d4c186cb2827d16ee5107448852aada205317a0f644d8ceac9e18";

// The amount to be transferred to the debitor.
$amount = 0.001;

// Create a new transaction with the parameters shown above.
$transaction = new Transaction($creditor, $private_key, $debitor, $amount);

/*
* Whether a transaction is valid can be checked using the isValid method. While it is possible to add the transaction directly 
* to the mempool, it will then be rejected. The isValid method gives a notification on an invalid transaction as to why it is invalid, 
* if valid returns TRUE.
*/
$response = $transaction->isValid();
// var_dump($response);

/*
* If the transaction is valid, it can be added to the mempool using the add method.
* More about the mempool is explained below.
*/
if ($transaction->isValid()) :
  $transaction->add();
endif;

/*
* In blockchain terminology, a mempool is a waiting area for the transactions that haven't been added to a block and are 
* still unconfirmed. This is how a Blockchain node deals with transactions that have not yet been included in a block.
* Note: After the unconfirmed transactions have been added to a block (mining), they are permanently deleted from the mempool.
*/
$mempool = new Mempool();

/*
* The get method can be used to see all current "waiting" transactions from the mempool.
*/
$unconfirmed_transactions = $mempool->get();

foreach ($mempool->get() as $transaction) :
  echo "tx: ", $transaction->tx, "<br>" . PHP_EOL;
  echo "timestamp: ", $transaction->timestamp, "<br>" . PHP_EOL;
  echo "amount: ", $transaction->amount, "<br>" . PHP_EOL;
  echo "from creditor: ", $transaction->creditor, "<br>" . PHP_EOL;
  echo "to debitor: ", $transaction->debitor, "<br>" . PHP_EOL;
  echo "<br><br>" . PHP_EOL;
endforeach;

// Mine a block with nodes
$node = new Node();

/*
* The node calculates a valid hash based on the difficulty. If successful, the block is returned with a valid hash.
*/
$new_block = $node->mining();
echo "Block was mined...", "<br>" . PHP_EOL;
echo "index: ", $new_block->index, "<br>" . PHP_EOL;
echo "hash: ", $new_block->hash, "<br>" . PHP_EOL;
echo "previous_hash: ", $new_block->previous_hash, "<br>" . PHP_EOL;
echo "count of transaction: ", count($new_block->transactions), "<br>" . PHP_EOL;
echo "nonce: ", $new_block->nonce, "<br>" . PHP_EOL;


/*
* The valid block can now be added to the blockchain. All transactions are validated and settled. 
* The mempool is then automatically emptied.
*/
$blockchain->addBlock($new_block);
/*
* Reminder: All transactions are now on the blockchain. In our example in the "blockchain.json"
*/
