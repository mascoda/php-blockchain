# ChangeLog

## Version 0.0.8 (28.10.2022)

- PHPDoc added

## Version 0.0.7 (10.10.2022)

- Refatcoring Storage

## Version 0.0.6 (19.10.2022)

- Added full documentation
- Added LICENSE
- Added VERSION Handler

## Version 0.0.1 (17.10.2022)

- Initial release
